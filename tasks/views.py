from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


# localhost:8000/tasks/create/
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/projects/")
    else:
        form = TaskForm()

    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


# localhost:8000/tasks/mine/
@login_required
def list_user_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/list_user_tasks.html", context)
